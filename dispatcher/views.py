from rest_framework import viewsets, mixins, generics
from .serializers import ClientSerializer, GlobalMailingStatisticSerializer, MailingSerializer, \
    MailingStatisticSerializer
from .models import Client, Message, Mailing
from django.db.models import Count
from .tasks import send_messages


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class GlobalMailingStatisticViewSet(mixins.ListModelMixin,
                                    viewsets.GenericViewSet):
    serializer_class = GlobalMailingStatisticSerializer

    def get_queryset(self):
        queryset = Message.objects.values('mailing_id', 'status').order_by('mailing_id').annotate(
            count=Count('status')
        )
        # подсчет количества встречающихся статусов для определенной рассылки
        # все равно что SELECT mailing_id, status, COUNT(status) AS count FROM Message GROUP BY mailing_id, status

        return queryset


class MailingStatisticViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = MailingStatisticSerializer

    def get_queryset(self):
        queryset = Message.objects.filter(mailing_id=self.kwargs['mailing_id'])
        return queryset


class MailingViewSet(viewsets.ModelViewSet):
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    def perform_create(self, serializer):
        mailing_id = Mailing.objects.all().count()
        send_messages.apply_async(args=[serializer.validated_data, mailing_id],
                                  eta=serializer.validated_data['start_date'],
                                  expires=serializer.validated_data['end_date'])  # Отправка задачи в Celery
        serializer.save()
