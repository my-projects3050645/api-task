from django.db import models
from django.core.validators import MinLengthValidator


# Сущность рассылка
class Mailing(models.Model):
    start_date = models.DateTimeField()  # Начало рассылки
    end_date = models.DateTimeField()  # Конец рассылки
    text = models.TextField()  # Текст сообщения
    filters = models.CharField(max_length=50, blank=True, null=True)
    # Фильтры клиентов, задаются через пробел с запятой (', ')

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'


# Сущность клиент
class Client(models.Model):
    mobile_phone = models.CharField(validators=[MinLengthValidator(5), ], max_length=20)  # Мобильный номер клиента
    mobile_operator_code = models.CharField(max_length=5)
    # Код оператора мобильного номера

    tag = models.CharField(blank=True, null=True, max_length=50, default='')  # Теги для фильтров рассылки
    utc = models.CharField(max_length=2)  # Часовой пояс клиента

    def __str__(self):
        return str(self.pk)

    class Meta:
        verbose_name = 'Клиент'
        verbose_name_plural = 'Клиенты'


# Сущность сообщения
class Message(models.Model):
    send_date = models.DateTimeField(auto_now_add=True)  # Дата отправки сообщения
    status = models.CharField(max_length=50)  # Результат отправки сообщения
    mailing_id = models.ForeignKey(Mailing, on_delete=models.CASCADE, related_name='mailing_id', verbose_name='mailing')
    # id рассылки

    client_id = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='mailing_id', verbose_name='client')
    # id клиента

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
        ordering = ['-send_date']
        default_related_name = 'Тест'
