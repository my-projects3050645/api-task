import datetime
from celery import shared_task
from .models import Message, Client, Mailing
import requests


@shared_task
def send_messages(data, mailing_id):
    index = 0

    if data['filters']:
        clients = Client.objects.filter(mobile_operator_code__in=data['filters'].split(', ')) | \
                  Client.objects.filter(tag__in=data['filters'].split(', '))
    # разделение фильтров производится по запятой и пробелу
    else:
        clients = Client.objects.all()

    while data['end_date'].timestamp() >= datetime.datetime.utcnow().timestamp() and index < len(clients):

        client_phone = clients[index].mobile_phone
        message_id = Message.objects.all().count()
        text = data['text']
        json = {'id': message_id, 'phone': client_phone, 'text': text}
        send_date = datetime.datetime.now()

        try:
            TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE3MzAzODIzMzMsIml" \
                    "zcyI6ImZhYnJpcXVlIiwibmFtZSI6Imh0dHBzOi8vdC5tZS9sdWNhc2gxMjMzMjEifQ" \
                    ".b9HBzd1ClyaAZyDropvJ7sdpUZInbB4IrRp2X00qkps"

            requests.post(f'https://probe.fbrq.cloud/v1/send/{message_id}',
                          headers={"Authorization": f"Bearer {TOKEN}"},
                          json=json,
                          timeout=10)

            status = "Отправлено"

        except requests.exceptions.Timeout:
            status = "Ошибка: время ожидания ответа вышло"

        except requests.exceptions.TooManyRedirects:
            status = "Ошибка: слишком много переадресаций"

        except requests.exceptions.RequestException:
            status = "Ошибка: неизвестная ошибка"

        mailing = Mailing.objects.get(id=mailing_id)
        message = Message(send_date=send_date, status=status, client_id=clients[index], mailing_id=mailing)
        message.save()

        index += 1
