from rest_framework import serializers
from .models import Client, Message, Mailing


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class GlobalMailingStatisticSerializer(serializers.ModelSerializer):
    mailing_id = serializers.IntegerField()
    count = serializers.IntegerField()

    class Meta:
        model = Message
        fields = ('mailing_id', 'status', 'count')


class MailingStatisticSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = '__all__'


class MailingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Mailing
        fields = '__all__'
