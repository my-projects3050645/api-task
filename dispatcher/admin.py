from django.contrib import admin
from .models import Mailing, Message, Client


# Register your models here.
class MailingAdmin(admin.ModelAdmin):
    list_display = ('start_date', 'end_date', 'text', 'filters')


class MessageAdmin(admin.ModelAdmin):
    list_display = ('send_date', 'status', 'mailing_id', 'client_id')


class ClientAdmin(admin.ModelAdmin):
    list_display = ('mobile_phone', 'mobile_operator_code', 'tag', 'utc')


admin.site.register(Message, MessageAdmin)
admin.site.register(Mailing, MailingAdmin)
admin.site.register(Client, ClientAdmin)
