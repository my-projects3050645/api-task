from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import ClientViewSet, GlobalMailingStatisticViewSet, MailingStatisticViewSet, MailingViewSet

router = DefaultRouter()

router.register('clients', ClientViewSet, basename='clients')
router.register(r'^statistics/(?P<mailing_id>[0-9]+)', MailingStatisticViewSet, basename='mailing')
router.register('statistics', GlobalMailingStatisticViewSet, basename='mailings')
router.register('mailings', MailingViewSet, basename='messages')

urlpatterns = [
    path('', include(router.urls))
]