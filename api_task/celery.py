import os
from celery import Celery

# Настройки celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'api_task.settings')

app = Celery('api_task', broker='amqp://guest@localhost//')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
